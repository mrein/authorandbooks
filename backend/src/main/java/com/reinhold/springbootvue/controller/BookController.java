package com.reinhold.springbootvue.controller;

import com.reinhold.springbootvue.model.Author;
import com.reinhold.springbootvue.model.Book;
import com.reinhold.springbootvue.repo.AuthorRepository;
import com.reinhold.springbootvue.repo.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping("/books")
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @PostMapping("/books")//TODO
//    public Book createBook(@Valid @RequestBody Book book, @PathVariable(value = "authorId") Long authorId) {
//        return authorRepository.findById(authorId).map(author -> {
//            book.setAuthor(author);
//            return bookRepository.save(book);
//        }).orElse()
//
//    }

    @GetMapping("/books/{year}")
    public List<Book> getAllBooksByYear(@PathVariable int year) {
        return bookRepository.findAllByYear(year);
    }

    @GetMapping("/book/{bookId}")
    public Book getBook(@PathVariable Long bookId) {
        return bookRepository.findById(bookId).orElseThrow(() -> new ResourceNotFoundException("BookId " + bookId + " not found"));
    }

}
