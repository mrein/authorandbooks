package com.reinhold.springbootvue.controller;

import com.reinhold.springbootvue.model.Author;
import com.reinhold.springbootvue.model.Book;
import com.reinhold.springbootvue.repo.AuthorRepository;
import com.reinhold.springbootvue.repo.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthorController {

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    BookRepository bookRepository;

    @GetMapping("/authors")
    public Page<Author> getAllAuthors(Pageable pageable) {
        return authorRepository.findAll(pageable);
    }

    @PostMapping("/authors")
    public Author createAuthor(@Valid @RequestBody Author author) {
        return authorRepository.save(author);
    }

    @GetMapping("/authors/{authorId}")
    public Author getAuthor(@PathVariable Long authorId) {
        return authorRepository.findById(authorId).orElseThrow(() -> new ResourceNotFoundException("AuthorId "
                + authorId + " not found "));
    }

    @GetMapping("/authors/{authorId}/books")
    public Page<Book> getAllBooksByAuthorId(@PathVariable(value = "authorId") Long authorId, Pageable pageable) {
        return bookRepository.findByAuthorId(authorId, pageable);
    }

    @PostMapping("/authors/{authorId}/books")
    public Book addNewBook(@PathVariable(value = "authorId") Long authorId, @Valid @RequestBody Book book) {
        return authorRepository.findById(authorId).map(author -> {
            book.setAuthor(author);
            return bookRepository.save(book);
        }).orElseThrow(() -> new ResourceNotFoundException("AuthorId " + authorId + " not found "));
    }

    @PutMapping("/authors/{authorId}/books/{bookId}")
    public Book updateBook(@PathVariable(value = "authorId") Long authorId, @PathVariable(value = "bookId") Long bookId, @Valid @RequestBody Book bookRequest) {
        if (!authorRepository.existsById(authorId)) {
            throw new ResourceNotFoundException("AuthorId " + authorId + " not found");
        }
        return bookRepository.findById(bookId).map(book -> {
            book.setDescription(bookRequest.getDescription());
            book.setYear(bookRequest.getYear());
            book.setTitle(bookRequest.getTitle());
            return bookRepository.save(book);
        }).orElseThrow(() -> new ResourceNotFoundException("BookId " + bookId + " not found"));
    }

    @DeleteMapping("/authors/{authorId}/books/{bookId}")
    public ResponseEntity<?> deleteBook(@PathVariable(value = "authorId") Long authorId, @PathVariable(value = "bookId") Long bookId) {
        if (!authorRepository.existsById(authorId)) {
            throw new ResourceNotFoundException("AuthorId " + authorId + " not found");
        }
        return bookRepository.findById(bookId).map(book -> {
            bookRepository.delete(book);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("BookId " + bookId + " not found"));
    }
}
