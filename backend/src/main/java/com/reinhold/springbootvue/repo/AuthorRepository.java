package com.reinhold.springbootvue.repo;

import com.reinhold.springbootvue.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

//    Author findAuthorByBooks(Book book);

    Author findById(long id);

    Author findByFirstName(String firstname);

    Author findByLastName(String lastname);


}
