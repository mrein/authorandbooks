package com.reinhold.springbootvue.repo;

import com.reinhold.springbootvue.model.Author;
import com.reinhold.springbootvue.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    Book findById(long id);

    List<Book> findAllByYear(int year);

    List<Book> findAllByAuthor(Author author);

    Page<Book> findByAuthorId(Long authorId, Pageable pageable);

    List<Book> findByAuthorId(Long authorId);



}
